// Author - Nitesh Ahlawat
// import dependencies
const express = require('express');
const path = require('path');

// set up expess validator
const { check, validationResult } = require('express-validator');

//setup the DB connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/onlinestore', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Setup the model for the order.
const Order = mongoose.model('Order', {
    name: String,
    email: String,
    phone: String,
    deliveryAddress: String,
    chocolate: String,
    milk: String,
    eggs: String,
    shippingCharges: String,
    subTotal: Number,
    tax: Number,
    total: Number
});

// set up variables
var myApp = express();
myApp.use(express.urlencoded({ extended: false }));

// set path to public folders, view folders and ejs view engine
myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');

// set up different routes (pages) of the website

//------------------ Validation functions ----------------------

var phoneRegex = /^[0-9]{10}$/;
var emailRegex = /^[A-Za-z]{3,40}[\@][A-Za-z]{3,30}[\.][A-Za-z]{3}$/;
var unitRegex = /^\#?[0-9]{1,5}$/;
var streetRegex = /^[A-Za-z]{3,40}$/;
var postalRegex = /^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/;

// function to check a userInput using regular expressions
function checkRegex(userInput, regex) {
    if (regex.test(userInput)) {
        return true;
    }
    else {
        return false;
    }
}

// Custom email validation function
function customEmailValidation(value) {
    if (!checkRegex(value, emailRegex)) {
        throw new Error('Please enter correct email format: email@domain.com');
    }
    return true;
}

// Custom phone validation function
function customPhoneValidation(value) {
    if (!checkRegex(value, phoneRegex)) {
        throw new Error('Please enter correct phone format: 1231231234');
    }
    return true;
}

// Custom unit number validation function
function customUnitValidation(value) {
    if (!checkRegex(value, unitRegex)) {
        throw new Error('Please enter Unit in correct format: #1234');
    }
    return true;
}

// Custom street name validation function
function customStreetValidation(value) {
    if (!checkRegex(value, streetRegex)) {
        throw new Error('Please enter Street name in correct format: xyz street');
    }
    return true;
}

// Custom postal code validation function
function customPostalValidation(value) {
    if (!checkRegex(value, postalRegex)) {
        throw new Error('Please enter Postal code in correct format: X9X 9X9');
    }
    return true;
}

// render the home page
myApp.get('/', function (req, res) {
    res.render('form');
});

myApp.post('/', [
    check('name', 'Name is required!').notEmpty(),
    check('email', '').custom(customEmailValidation),
    check('phone', '').custom(customPhoneValidation),
    check('unit', '').custom(customUnitValidation),
    check('street', '').custom(customStreetValidation),
    check('city', 'Must enter City name').notEmpty(),
    check('province', 'Must Select a Province').notEmpty(),
    check('postcode', '').custom(customPostalValidation),
    check('delivery', 'Must Select Delivery Time').notEmpty()

], function (req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors); // check what is the structure of errors
        res.render('form', {
            errors: errors.array()
        });
    }
    else {
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var unit = req.body.unit;
        var street = req.body.street;
        var city = req.body.city;
        var province = req.body.province;
        var postcode = req.body.postcode;
        var chocolate = req.body.chocolate;
        var milk = req.body.milk;
        var eggs = req.body.eggs;
        var delivery = req.body.delivery;

        chocolate = parseInt(chocolate);
        milk = parseInt(milk);
        eggs = parseInt(eggs);

        var subTotal = 0;
        var itemsPurchased = '';

        itemsPurchased += `<tr style="font-weight: bold;"><td>Item</td><td>Quantity</td><td>Unit Price</td><td>Total Price</td></tr>`;

        if (chocolate > 0) {
            subTotal += chocolate * 2;
            itemsPurchased += `<tr><td>Chocolate</td><td>${chocolate}</td><td>$2</td><td>$${chocolate * 2}</td></tr>`;
        }

        if (milk > 0) {
            subTotal += milk * 4;
            itemsPurchased += `<tr><td>Milk</td><td>${milk}</td><td>$4</td><td>$${milk * 4}</td></tr>`;
        }

        if (eggs > 0) {
            subTotal += eggs * 6;
            itemsPurchased += `<tr><td>Eggs</td><td>${eggs}</td><td>$6</td><td>$${eggs * 6}</td></tr>`;
        }

        var shippingCharges = 0;
        switch (delivery) {
            case "1 Days":
                shippingCharges = 20;
                break;
            case "2 Days":
                shippingCharges = 15;
                break;
            case "3 Days":
                shippingCharges = 10;
                break;
            case "4 Days":
                shippingCharges = 5;
                break;
            case "5 Days":
                shippingCharges = 0;
                break;
        }

        subTotal += shippingCharges;
        itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Shipping Charges (${delivery})</td><td>$${shippingCharges}</td></tr>`;

        if (subTotal < 10) {
            var minimumPurchase = 'Minimum Purchase should be $10';
            res.render('form', { minimumPurchase });
        }
        else {
            var taxPercentage = 0;
            switch (province) {
                case "Alberta":
                    taxPercentage = 0.05;
                    break;
                case "British Columbia":
                    taxPercentage = 0.12;
                    break;
                case "Manitoba":
                    taxPercentage = 0.12;
                    break;
                case "New Brunswick":
                    taxPercentage = 0.15;
                    break;
                case "Newfoundland and Labrador":
                    taxPercentage = 0.15;
                    break;
                case "Northwest Territories":
                    taxPercentage = 0.05;
                    break;
                case "Nova Scotia":
                    taxPercentage = 0.15;
                    break;
                case "Nunavut":
                    taxPercentage = 0.05;
                    break;
                case "Ontario":
                    taxPercentage = 0.13;
                    break;
                case "Prince Edward Island":
                    taxPercentage = 0.15;
                    break;
                case "Quebec":
                    taxPercentage = 0.14975;
                    break;
                case "Saskatchewan":
                    taxPercentage = 0.11;
                    break;
                case "Yukon":
                    taxPercentage = 0.05;
                    break;
            }
            var tax = subTotal * taxPercentage;
            var total = subTotal + tax;

            itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Sub Total</td><td>$${subTotal}</td></tr>`;
            itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Tax ${taxPercentage * 100}%</td><td>$${tax.toFixed(2)}</td></tr>`;
            itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Total </td><td>$${total.toFixed(2)}</td></tr>`;

            var pageData = {
                name: name,
                email: email,
                phone: phone,
                deliveryAddress: unit + ", " + street + ", " + city + ", " + province + ", " + postcode,
                shippingCharges: shippingCharges,
                subTotal: subTotal,
                tax: tax,
                total: total,
                itemsPurchased: itemsPurchased
            }

            // create an object for the model Order
            var myOrder = new Order({
                name: name,
                email: email,
                phone: phone,
                deliveryAddress: unit + ", " + street + ", " + city + ", " + province + ", " + postcode,
                chocolate: `Quantity = ${chocolate} Price = $${chocolate * 2}`,
                milk: `Quantity = ${milk} Price = $${milk * 4}`,
                eggs: `Quantity = ${eggs} Price = $${eggs * 6}`,
                shippingCharges: `${delivery} $${shippingCharges}`,
                subTotal: subTotal,
                tax: tax,
                total: total
            });
            // save the order
            myOrder.save().then(function () {
                console.log('New order created');
            });

            //Display the output on form
            res.render('form', pageData);
        }
    }
});


//All Orders Page
myApp.get('/allorders', function (req, res) {
    Order.find({}).exec(function (err, orders) {
        res.render('allorders', { orders: orders });
    });
});

//author page
myApp.get('/author', function (req, res) {
    res.render('author', {
        name: 'Nitesh Ahlawat',
        studentNumber: '8779849'
    });
});

// start the server and listen at a port
myApp.listen(8080); // Open URL in Browser: http://localhost:8080

//Confirmation Output
console.log('Everything executed fine.. website at port 8080!');